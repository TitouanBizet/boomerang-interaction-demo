# Boomerang interaction demo

A simple demo of “[boomerang](https://hci-museum.lisn.upsaclay.fr/boomerang)” interaction.

## Try it

[Here](https://openprocessing.org/sketch/2186665) is an openprocessing page with an adapted implementation.

## Getting started

```
npm i
npm run dev
```
