const offset = { x: 0, y: 0 };

function newBoomerang(): { boomerang: HTMLElement, boomerangContent: HTMLElement } {
	const boomerang = document.createElement("div");
	boomerang.classList.add("boomerang");

	const boomerangContent = document.createElement("div");
	boomerangContent.classList.add("boomerang-content");
	boomerangContent.draggable = true;
	boomerang.appendChild(boomerangContent);

	document.body.appendChild(boomerang);

	return { boomerang, boomerangContent };
}

export function dragstartHandler(event: DragEvent): void {
	const target = event.target as HTMLElement;
	event.dataTransfer?.setData("text/plain", target.outerHTML);

	const droppedElement = event.target as HTMLElement;
	const droppedRectangle = droppedElement.getBoundingClientRect();

	offset.x = event.clientX - droppedRectangle.x;
	offset.y = event.clientY - droppedRectangle.y;
}

export function dragendHandler(event: DragEvent): void {
	if (event.dataTransfer?.dropEffect === "move") {
		const droppedElement = event.target as HTMLElement;
		if (droppedElement.parentElement?.parentElement?.className === "boomerang")
			droppedElement.parentElement.parentElement.remove();
		else
			droppedElement.remove();
	}
}

export function moveHandler(event: DragEvent): void {
	// Mandatory to make ondrop work
	event.preventDefault();
}

export function dropHandler(event: DragEvent): void {
	const draggedData = event.dataTransfer?.getData("text/plain");
	if (draggedData) {
		const { boomerang, boomerangContent } = newBoomerang();
		boomerangContent.innerHTML = draggedData;

		const draggableElement = boomerang.firstElementChild as HTMLElement;
		draggableElement.ondragstart = dragstartHandler;
		draggableElement.ondragend = dragendHandler;

		boomerang.style.left = `${event.clientX - offset.x}px`;
		boomerang.style.top = `${event.clientY - offset.y}px`;

		event.dataTransfer!.dropEffect = "move";
	}
}
