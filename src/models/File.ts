import { dragstartHandler, dragendHandler } from "@/boomerang";

export class File {
	private static extensionSamples = ["cs", "exe", "gif", "tar.gz", "jar", "jpg", "rar", "wav", "iso", "docx", "png", "svg", "blend", "apk", "pptx", "c", "php", "r", "sh", "mp3", "midi", "c", "java", "js", "csv", "gif", "avi", "mkv", "xml", "html", "json", "yaml", "svelte", "vue", "cpp", "ts", "pdf", "odt", "odp", "txt"];

	public constructor(
		public readonly name: string
	) { }

	public static withRandomExtension(fileName: string): File {
		const randomExtension = File.extensionSamples[Math.floor(Math.random() * File.extensionSamples.length)];
		return new File(`${fileName}.${randomExtension}`);
	}

	public get html(): HTMLElement {
		const li = document.createElement("div");
		li.classList.add("item", "file");
		li.draggable = true;
		li.ondragstart = dragstartHandler;
		li.ondragend = dragendHandler;

		const icon = document.createElement("img");
		icon.src = "/svg/file.svg";
		icon.draggable = false;
		li.append(icon);

		const label = document.createElement("span");
		label.innerText = this.name;
		li.append(label);

		return li;
	}
}
