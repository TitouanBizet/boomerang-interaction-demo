export class Folder {
	private icon = document.createElement("img");

	public constructor(
		public readonly name: string,
	) { }

	public get html(): HTMLElement {
		const li = document.createElement("div");
		li.classList.add("item", "folder");
		li.ondragenter = () => this.onDragenter();
		li.ondragover = event => this.onDragover(event);
		li.ondragleave = () => this.onDragleave();
		li.ondrop = event => this.onDrop(event);

		this.icon.draggable = false;
		this.icon.src = "/svg/folder.svg";
		li.append(this.icon);

		const label = document.createElement("span");
		label.innerText = this.name;
		li.append(label);

		return li;
	}

	private onDragenter(): void {
		this.icon.src = "/svg/open-folder.svg";
	}

	private onDragleave(): void {
		this.icon.src = "/svg/folder.svg";
	}

	private onDragover(event: DragEvent): void {
		this.icon.src = "/svg/open-folder.svg";
		event.preventDefault();
	}

	private onDrop(event: DragEvent): void {
		this.icon.src = "/svg/folder.svg";

		if (event.dataTransfer?.getData("text/plain")) {
			event.dataTransfer.dropEffect = "move";
		}
	}
}
