import './style/main.css';
import './style/boomerang.css';

import { dropHandler, moveHandler } from "./boomerang";

import { File } from "./models/File";
import { Folder } from "./models/Folder";

import skyrimData from "./data/skyrimLocations.json";

const folders = Object.keys(skyrimData).map(holdName => new Folder(holdName));
const files = Object.values(skyrimData).flatMap(locationNames => locationNames.map(locationName => File.withRandomExtension(locationName)));

const main = document.querySelector("#files");

document.querySelectorAll(".boomerang-container").forEach(container => {
	if (container instanceof HTMLElement) {
		container.ondragover = moveHandler;
		container.ondrop = dropHandler;
	}
});

if (main != null) {
	folders.forEach(folder => main.append(folder.html));
	files.forEach(file => main.append(file.html));
}
